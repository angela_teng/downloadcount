from google.appengine.ext import webapp
import connectdb as d

class CORSEnabledHandler(webapp.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        #self.response.headers['Content-Type'] = 'text/csv'
        #self.response.out.write(self.dump_csv())
        c = d.Counter.gql("WHERE id = :1","doctor")
        i = c.get() 
        i.count = i.count+1
        i.put() 
        self.response.out.write(i.count)         

#increase number
app =webapp.WSGIApplication([('/increase', CORSEnabledHandler)])
