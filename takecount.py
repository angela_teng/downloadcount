import connectdb as d
from google.appengine.ext import webapp

class TakeCount(webapp.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        c = d.Counter.gql("WHERE id = :1","doctor")
        i = c.get()
        self.response.out.write(i.count) 


takecount = webapp.WSGIApplication([('/', TakeCount)])